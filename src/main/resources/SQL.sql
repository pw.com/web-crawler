CREATE TABLE public.test_user (
  id INTEGER PRIMARY KEY NOT NULL DEFAULT nextval('test_user_id_seq'::regclass),
  name CHARACTER VARYING(20),
  age INTEGER
);