package com.pw.common.entity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 对应网址 http://www.kuaidaili.com/free/inha/1489/
 * Created by pw on 2017/2/15.
 */
public class KuaiDaiLi {

    //    IP	PORT	匿名度	类型	位置	响应速度	最后验证时间
    private String ip;
    private String post;
    private String anonymous;
    private String type;
    private String location;
    private int responseSpeed;
    private Date lastTime;

    /**
     * @param responseSpeed 超时时间
     * @param day           几天前
     * @return
     */
    public boolean isNormal(int responseSpeed, int day) {
        if (null == this.getLastTime()) return false;
        if (this.responseSpeed > responseSpeed) return false;

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_WEEK, 0 - day);
        long startTime = cal.getTime().getTime();
        long time = this.getLastTime().getTime();

        if (time < startTime) return false;
        return true;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(String anonymous) {
        this.anonymous = anonymous;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getResponseSpeed() {
        return responseSpeed;
    }

    public void setResponseSpeed(int responseSpeed) {
        this.responseSpeed = responseSpeed;
    }

    public Date getLastTime() {
        return lastTime;
    }

    public void setLastTime(String lastTime) {
        this.lastTime = DateUtil.getDate(lastTime);
    }


}

class DateUtil {

    static DateFormat format;
    private final static String FORMAT = "yyyy-MM-dd HH:mm:ss";

    static {
        format = new SimpleDateFormat(FORMAT);
    }

    public static Date getDate(String time) {
        try {
            return format.parse(time);
        } catch (Exception e) {
            System.err.println(e);
        }
        return null;
    }

//    public static void main(String[] args) {
//        long time = getDate("2017-02-15 02:33:39").getTime();
//        Calendar cal = Calendar.getInstance();
//        long startTime = cal.getTime().getTime();
//        cal.add(Calendar.DAY_OF_WEEK, -1);
//        long endTime = cal.getTime().getTime();
//        if (time > endTime && time < startTime) {
//            System.out.println("时间Ok");
//        }
//    }



}
