package com.pw.common.util;

import cn.edu.hfut.dmic.webcollector.model.CrawlDatums;
import cn.edu.hfut.dmic.webcollector.model.Page;
import cn.edu.hfut.dmic.webcollector.plugin.berkeley.BreadthCrawler;
import com.pw.tieba.test.TieBaCrawler;
import org.jsoup.select.Elements;

/**
 * Created by pw on 2017/2/15.
 */
public class KuaiDaiLi extends BreadthCrawler {

    private final static String URL = "http://www.kuaidaili.com/free/";

    static TieBaCrawler crawler;

    static {
        crawler = new TieBaCrawler("kuaidaili", "www.kuaidaili.com");
    }

    /**
     * @param crawlPath    用于维护URL的文件夹
     * @param downloadPath 用于保存图片的文件夹
     */
    public KuaiDaiLi(String crawlPath, String downloadPath) {
        super(crawlPath, true);
    }

    public void visit(Page page, CrawlDatums next) {
        System.out.println(page.getHtml());
        Elements elements = page.getDoc().getElementsByTag("a");
        for (int i = 0 ; i < elements.size() ; i++){
            String strUrl = elements.get(i).attr("href");
            if(!"#".equals(strUrl)){
                if(strUrl.indexOf("http://www.kuaidaili.com/free/") != -1 ){
//                    System.out.println(strUrl);
                }else {
                    System.out.println(URL + strUrl);
                }
            }
        }
    }

    public static void main(String[] args) throws Exception {
        crawler.addSeed(URL);
        crawler.addRegex("http://www.kuaidaili.com/free/*");
        crawler.setThreads(50);

        crawler.start(5);
    }
}
