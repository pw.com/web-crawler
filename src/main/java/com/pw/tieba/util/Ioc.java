package com.pw.tieba.util;

import com.pw.tieba.core.Crawler;
import com.pw.tieba.dao.ProxyDao;
import com.pw.tieba.service.ProxyService;

/**
 * Created by pw on 2017/3/19.
 */
public class Ioc {

    public static ProxyDao proxyDao = new ProxyDao();

    public static ProxyService proxyService = new ProxyService();

    public static Crawler crawler = new Crawler();

}
