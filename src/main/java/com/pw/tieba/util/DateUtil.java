package com.pw.tieba.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by pw on 2017/3/19.
 */
public class DateUtil {

    /**
     *  yyyy-MM-dd
     */
    public static final String yyyyMMdd ="yyyy-MM-dd";

    private DateUtil(){

    }

    public static String dateToString(Date time, String key ){
        return new SimpleDateFormat(key).format(time);
    }

    public static Date stringToDate(String time, String key){
        try {
            return new SimpleDateFormat(key).parse(time);
        }catch (Exception e){

        }
        return null;
    }



}
