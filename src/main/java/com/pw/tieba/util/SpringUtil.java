package com.pw.tieba.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by pw on 2017/4/22.
 */
public class SpringUtil {

    private static ApplicationContext ctx;

    private static JdbcTemplate jdbcTemplate;

    private static SpringUtil springUtil;

    private SpringUtil() {
        ctx = new ClassPathXmlApplicationContext("spring-config.xml");
        jdbcTemplate = (JdbcTemplate) ctx.getBean("jdbcTemplate");
    }

    public static SpringUtil getSpringUtil() {
        return springUtil = null == springUtil ? new SpringUtil() : springUtil;
    }


    public static  <T> T getBean(Class<T> requiredType) {
        return ctx.getBean(requiredType);
    }

    public static Object getBean(String className) {
        return ctx.getBean(className);
    }

    public static JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }
}
