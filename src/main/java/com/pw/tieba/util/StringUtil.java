package com.pw.tieba.util;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by pw on 2017/3/23.
 */
public class StringUtil {
    /**
     * 中文赚Unicode
     *
     * @param s
     * @return
     */
    public static String ChineseToUnicode(String s) {
        String as[] = new String[s.length()];
        String s1 = "";
        for (int i = 0; i < s.length(); i++) {
            as[i] = Integer.toHexString(s.charAt(i) & 0xffff);
            s1 = s1 + "\\u" + as[i];
        }
        return s1;
    }

    /**
     * Unicode转中文
     *
     * @param string
     * @return
     */
    public static String UnicodeToChinese(String string) {
        String str = string.replace("\\u", ",");
        String[] s2 = str.split(",");
        String s1 = "";
        for (int i = 1; i < s2.length; i++) {
            s1 = s1 + (char) Integer.parseInt(s2[i], 16);
        }
        return s1.trim().length() == 0 ? string : s1;
    }

    public static List getEmailList(String str) {
        //在此维护域名后缀表
        String dn = "com|cn|org|com.cn|xyz|net|gg|gov.cn|love";
        Pattern p = Pattern.compile("[\\w[.-]]+@[\\w[.-]]+\\.(" + dn + ")"); //邮箱验证
        Matcher m = p.matcher(str);
        List<String> emailList = new ArrayList<String>();
        while (m.find()) {
            //去除包涵连续两个点的邮箱
            if(!m.group().contains("..")) {
                emailList.add(m.group());
            }
        }
        return emailList;
    }

    public static Map<String,Object> jsonUnicodeToChinese(Map<String,Object> json){
        if(null != json && json.size() > 0){
            for(String key : json.keySet()){
                json.put(key,StringUtil.UnicodeToChinese(json.get(key).toString()));
            }
            return json;
        }
        return null;
    }






}
