package com.pw.tieba.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.pw.tieba.core.Cache;
import com.pw.tieba.dao.ProxyDao;
import com.pw.tieba.entity.Proxy;
import com.pw.tieba.entity.ProxyShowApi;
import com.pw.tieba.util.DateUtil;
import com.pw.tieba.util.Ioc;
import com.show.api.ShowApiRequest;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.*;

/**
 * Created by pw on 2017/3/19.
 */
public class ProxyService {

    private static Logger logger = Logger.getLogger(ProxyService.class);

    private ProxyDao proxyDao = Ioc.proxyDao;

    public void getShowApiProxy(){
        List<Proxy> list = new ArrayList<Proxy>();
        String res = new ShowApiRequest("http://route.showapi.com/22-1", "33918", "8c36b05e4c574590a56587b4ae3ade80").post();
        Map<String, Object> resMap = JSON.parseObject(res, new TypeReference<Map<String, Object>>(){} );
        Object code = resMap.get("showapi_res_code");

        logger.info("showapi_res_code:" +code);
        if(null != code && code.equals(0)){
            Map<String, Object> bodyMap = JSON.parseObject(resMap.get("showapi_res_body").toString(), new TypeReference<Map<String, Object>>(){} );
            Object retCode = bodyMap.get("ret_code");
            logger.info("showapi_res_body->ret_code:" +retCode);
            if(null != retCode && retCode.equals(0)){
                Map<String, Object> beanMap = JSON.parseObject(bodyMap.get("pagebean").toString(), new TypeReference<Map<String, Object>>(){} );
                List<ProxyShowApi> proxyList = JSON.parseArray(beanMap.get("contentlist").toString(), ProxyShowApi.class);
                if(null != proxyList && proxyList.size() > 0){
                    for(ProxyShowApi showApiProxy:proxyList){
                        list.add(showApiProxy.getProxy());
                    }
                }
            }
        }
        Cache.LIST_PROXY.addAll(list);
//        proxyDao.save(list);
    }

    public void getXiciDaili(){
        List<Proxy> list = new ArrayList<Proxy>();
        for(int index = 1 ; index <= 10 ;index++){
//            String url = "http://www.xicidaili.com/nn/"+index;
//            org.openqa.selenium.Proxy httpProxy = new org.openqa.selenium.Proxy();
////            httpProxy.setHttpProxy("124.88.67.14:834");
//            DesiredCapabilities capabilities = DesiredCapabilities.htmlUnit();
//            capabilities.setCapability(CapabilityType.PROXY, httpProxy);
//            HtmlUnitDriver driver = new HtmlUnitDriver(capabilities){
//                @Override
//                protected WebClient modifyWebClient(WebClient client)
//                {
//                    DefaultCredentialsProvider creds = new DefaultCredentialsProvider();
//                    client.setCredentialsProvider(creds);
//                    return client;
//                }
//            };
////            driver.get(url);
//            driver.setJavascriptEnabled(true);
//            driver.get(url);
////            System.out.println(driver.getPageSource());
//            Proxy proxy;
//            List<WebElement> trs = driver.findElements(By.tagName("tr"));

            String url = "http://www.xicidaili.com/nn/"+index;
            HtmlUnitDriver driver = new HtmlUnitDriver();
            driver.setJavascriptEnabled(true);
            driver.get(url);

            Proxy proxy;
            List<WebElement> trs = driver.findElements(By.tagName("tr"));

            for(WebElement tr:trs){
                proxy = new Proxy();
                List<WebElement> tds = tr.findElements(By.tagName("td"));
                for(int i = 1 ; i < tds.size();i++){
                    switch (i){
                        case 1:
                            proxy.setIp(tds.get(i).getText());
                            break;
                        case 2:
                            proxy.setPort(Integer.parseInt(tds.get(i).getText()));
                            break;
                        case 3:
                            proxy.setAddress(tds.get(i).getText());
                            break;
                        case 4:
                            proxy.setIsAnonymity(tds.get(i).getText());
                            break;
                        case 5:
                            proxy.setType(tds.get(i).getText());
                            break;
                        case 6:
                            String speed = tds.get(i).findElement(By.tagName("div")).getAttribute("title");
                            proxy.setSpeed(speed.substring(0,speed.length()-1));
                            break;
                        case 9:
                            proxy.setLastTime(DateUtil.stringToDate(tds.get(i).getText(),"yy-MM-dd HH:mm"));
                            break;
                    }
                }
                if(null != proxy.getIp()){
                    list.add(proxy);
                }
            }
        }
        Cache.LIST_PROXY.addAll(list);
//        proxyDao.save(list);
    }

}
