package com.pw.tieba.task;

import com.pw.tieba.core.Cache;
import com.pw.tieba.util.Ioc;

/**
 * Created by pw on 2017/3/23.
 * 定时任务
 *   定时抓取代理
 */
public class TaskProxy implements Runnable  {
    public void run() {
        System.out.println("-------------------TaskProxy start ----------------------------");
        Cache.LIST_PROXY.clear();
        Ioc.proxyService.getShowApiProxy();
        Ioc.proxyService.getXiciDaili();
        Cache.refreshProxys();
        Ioc.proxyDao.save(Cache.LIST_PROXY);
        System.out.println("-------------------TaskProxy End   ----------------------------");
    }
}
