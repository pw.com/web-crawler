package com.pw.tieba.task;

import com.pw.tieba.util.Ioc;

import java.util.Scanner;

/**
 * Created by pw on 2017/3/23.
 * 程序结束执行事件
 */
public class TaskExit extends Thread {

    public TaskExit() {
        doShutDownWork();
    }

    private void doShutDownWork() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                try {
                    //TODO 添加结束程序事件
                } catch (Exception ex) {
                    System.err.println(ex);
                }

            }
        });
    }

    public void run() {
        while (true) {
            try {
                Scanner sc = new Scanner(System.in);
                String str = sc.nextLine();
                if (null != str && str.length() > 0) {
                    switch (str) {
                        case "exit":
                            System.exit(0);
                            break;
                        case "start": //开始爬虫
                            Ioc.crawler.init();
                            Ioc.crawler.start(99999);
                            break;
                        case "stop"://结束爬虫
                            Ioc.crawler.stop();
                            break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
