package com.pw.tieba.task;

import com.pw.tieba.config.Config;

import java.util.concurrent.*;

/**
 * Created by pw on 2017/3/24.
 */
public class TimingTask {


    public void start(){
        initTaskExit();
        initTaskProxy();
    }


    private void initTaskProxy(){
        TaskProxy task = new TaskProxy();
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        service.scheduleAtFixedRate(task, 0, Config.TASK_PROXY_TIME_SECONDS, TimeUnit.SECONDS);
    }
    private void initTaskExit(){
        new TaskExit().start();
    }

}
