package com.pw.tieba.entity;

/**
 * Created by pw on 2017/4/22.
 * {"firstClass":"文学","id":708438,"infoJson":"{\"second_class\":\"都市·言情小说\",\"name\":\"崛\",\"id\":\"708438\",\"first_class\":\"文学\"}","name":"崛","secondClass":"都市·言情小说"}
 */
public class Forum {

    private Integer id;
    private String name;
    private String firstClass;
    private String secondClass;

    private String infoJson;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstClass() {
        return firstClass;
    }

    public void setFirstClass(String firstClass) {
        this.firstClass = firstClass;
    }

    public String getSecondClass() {
        return secondClass;
    }

    public void setSecondClass(String secondClass) {
        this.secondClass = secondClass;
    }

    public String getInfoJson() {
        return infoJson;
    }

    public void setInfoJson(String infoJson) {
        this.infoJson = infoJson;
    }
}
