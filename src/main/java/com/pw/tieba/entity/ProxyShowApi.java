package com.pw.tieba.entity;

import java.util.Date;

/**
 * Created by pw on 2017/3/19.
 */
public class ProxyShowApi {


    private Date checkTime;
    private int port;
    private String pos ;
    private String ip;
    private String anoy;
    private String speed;

    public Proxy getProxy(){
        Proxy proxy = new Proxy();
        proxy.setIp(this.ip);
        proxy.setAddress(this.anoy);
        proxy.setSpeed(this.speed);
        proxy.setLastTime(this.checkTime);
        proxy.setIsAnonymity(this.anoy);
        proxy.setPort(this.port);
        return proxy;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getAnoy() {
        return anoy;
    }

    public void setAnoy(String anoy) {
        this.anoy = anoy;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }
}
