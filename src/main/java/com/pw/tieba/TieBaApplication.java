package com.pw.tieba;

import com.pw.tieba.core.Init;
import com.pw.tieba.task.TimingTask;
import com.pw.tieba.util.Ioc;
import com.pw.tieba.util.SpringUtil;

/**
 * Created by pw on 2017/3/24.
 */
public class TieBaApplication {

    public static void main(String[] args) {
        try {
            SpringUtil.getSpringUtil();
            new Init();
            new TimingTask().start();
            Thread.sleep(6000);

            Ioc.crawler.init();
        } catch (Exception e) {
            System.err.println(e);
        }
        System.out.println("--------------------------- Start :"+System.currentTimeMillis()+" --------------------------------------");
    }


}
