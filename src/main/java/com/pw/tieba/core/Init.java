package com.pw.tieba.core;

import com.pw.tieba.config.Config;

import java.util.Properties;

/**
 * Created by pw on 2017/3/23.
 */
public class Init {

    public Init(){
        initConfig();
    }

    /**
     * 加载配置
     */
    private void initConfig(){
        try {
            Properties pps = new Properties();
            pps.load(Init.class.getClassLoader().getResourceAsStream("crawler.properties"));

            Config.TASK_PROXY_TIME_SECONDS =  Integer.parseInt(pps.getProperty("task_proxy_time_seconds"));
            Config.CRAWLER_THREAD_NUMBER =  Integer.parseInt(pps.getProperty("crawler_thread_number"));

            Config.DATA_STORAGE =  pps.getProperty("crawler_data_storage");
            Config.CRAWLER_DATA_STORAGE_FILE_SUFFIX =  pps.getProperty("crawler_data_storage_file_suffix");
            Config.CRAWLER_URL_PATH =  pps.getProperty("crawler_get_url_path");
            Config.CRAWLER_URL =  pps.getProperty("crawler_url");

        }catch (Exception e){
            System.err.println(e);
        }
    }



}
