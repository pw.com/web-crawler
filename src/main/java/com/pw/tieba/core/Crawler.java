package com.pw.tieba.core;

import cn.edu.hfut.dmic.webcollector.model.*;
import cn.edu.hfut.dmic.webcollector.net.HttpRequest;
import cn.edu.hfut.dmic.webcollector.net.HttpResponse;
import cn.edu.hfut.dmic.webcollector.plugin.berkeley.BreadthCrawler;
import com.pw.tieba.config.Config;
import org.jsoup.select.Elements;

/**
 * Created by pw on 2017/3/25.
 */
public class Crawler extends BreadthCrawler {


    private Crawler crawler;

    public Crawler() {
        super(Config.CRAWLER_URL_PATH, true);
    }


    @Override
    public HttpResponse getResponse(CrawlDatum crawlDatum) throws Exception {
        HttpRequest request = new HttpRequest(crawlDatum);
        System.out.println("--------------> Cache.proxys.nextRandom()");
//        request.setProxy(Cache.proxys.nextRandom());
        return request.getResponse();
    }


    public void visit(Page page, CrawlDatums next) {
        Elements elements = page.getDoc().getElementsByTag("a");
        for (int i = 0; i < elements.size(); i++) {
            String strUrl = elements.get(i).attr("href");
            if (!"#".equals(strUrl)) {
                if (strUrl.indexOf(Config.CRAWLER_URL) != -1) {
                    next.add(strUrl);
                } else if (strUrl.indexOf("http") == -1) {
                    next.add(Config.CRAWLER_URL + strUrl);
                }
            }
        }
    }



    public void init() {
        crawler.addSeed(Config.CRAWLER_URL);
        crawler.setThreads(1);
        crawler.setResumable(true);
    }


}
