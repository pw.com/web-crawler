package com.pw.tieba.core;

import cn.edu.hfut.dmic.webcollector.net.Proxys;
import com.pw.tieba.entity.Proxy;

import java.util.*;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.TransferQueue;

/**
 * Created by pw on 2017/2/22.
 */
public class Cache {

    public final static String CACHE_FILE_PATH = "E:/";

    /* 缓存迟最大值 */
    public final static int CACHE_DATA_LENGTH_MAX = 10000;

    public final static Map<String,String> CONFIG = new HashMap<String, String>();

    public static List<Proxy> LIST_PROXY = new ArrayList<Proxy>();

    public static Proxys proxys= new Proxys();





    public static void refreshProxys(){
        if(LIST_PROXY.size() > 0){
            System.out.println("--->Cache -> refreshProxys Start");
            proxys.clear();
            for(Proxy proxy:LIST_PROXY){
                if(null != proxy.getIp()){
                    proxys.add(proxy.getIp(),proxy.getPort());
                }
            }
            System.out.println("--->Cache -> refreshProxys End");
        }
    }
}
