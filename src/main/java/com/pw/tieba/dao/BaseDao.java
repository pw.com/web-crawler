package com.pw.tieba.dao;

import com.pw.tieba.config.Config;
import com.pw.tieba.service.ProxyService;
import com.pw.tieba.util.DateUtil;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.Date;

/**
 * Created by pw on 2017/3/19.
 */
public class BaseDao {

    private static Logger logger = Logger.getLogger(ProxyService.class);


    protected void write(String path, String content) {
        try {
            String fileName = DateUtil.dateToString(new Date(),DateUtil.yyyyMMdd);
            String mkdir = Config.DATA_STORAGE + "/" +path+"/"+ fileName;
            File file = new File(mkdir);
            if(!file.exists())
                file.mkdirs();
            fileName = mkdir + "/" + System.currentTimeMillis()+Config.CRAWLER_DATA_STORAGE_FILE_SUFFIX;
            BufferedWriter output = new BufferedWriter(new FileWriter(new File(fileName), true));
            output.write(content);
            output.close();
            System.out.println("BaseDao File Write :" + fileName);
        } catch (Exception e) {
            logger.error(e);
        }
    }
}
