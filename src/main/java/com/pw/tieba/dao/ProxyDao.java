package com.pw.tieba.dao;

import com.alibaba.fastjson.JSON;
import com.pw.tieba.entity.Proxy;

import java.util.List;

/**
 * Created by pw on 2017/3/19.
 */
public class ProxyDao extends BaseDao {

    public void save(List<Proxy> list){
        if(null != list && list.size() > 0){
            StringBuffer str = new StringBuffer();
            for(Proxy proxy : list){
                str.append(JSON.toJSONString(proxy)+"\n");
            }
            this.write(Proxy.class.getSimpleName(),str.toString());
        }
    }

}
