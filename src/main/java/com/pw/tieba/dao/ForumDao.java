package com.pw.tieba.dao;

import com.pw.tieba.entity.Forum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Created by pw on 2017/4/22.
 */
@Repository
public class ForumDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public int inset(Forum forum) {
        Forum f = select(forum.getId());
        if (null == f) {
            String SQL = "INSERT INTO forum (id, name, first_class, second_class, info_json) VALUES (?, ?, ?, ?, ?)";
            return jdbcTemplate.update(SQL, new Object[]{forum.getId(), forum.getName(), forum.getFirstClass(), forum.getSecondClass(), forum.getInfoJson()});
        }
        return -1;
    }

    public Forum select(int id) {
        try {
            String SQL = "SELECT * FROM forum WHERE id =? ";
            return (Forum) jdbcTemplate.queryForObject(SQL, new Object[]{id}, new BeanPropertyRowMapper(Forum.class));
        }catch (Exception e){
            System.err.println(e);
        }
        return null;
    }
}
