package com.pw.tieba.config;

/**
 * Created by pw on 2017/3/23.
 */
public class Config {


    /**
     * 定时爬行代理周期
     */
    public static int TASK_PROXY_TIME_SECONDS = 60;
    /**
     * 数据储存位置
     */
    public static String DATA_STORAGE = "";
    /**
     * 爬虫爬取URL 存放位置
      */
    public static String CRAWLER_URL_PATH = "";
    /**
     * 文件存储后缀
     */
    public static String CRAWLER_DATA_STORAGE_FILE_SUFFIX = "";
    /**
     * 爬取URL
     */
    public static String CRAWLER_URL = "";
    /**
     * 爬虫线程数量
     */
    public static int CRAWLER_THREAD_NUMBER = 0;

//
//    @Value("task_proxy_time_seconds")
//    public static void setTaskProxyTimeSeconds(int taskProxyTimeSeconds) {
//        TASK_PROXY_TIME_SECONDS = taskProxyTimeSeconds;
//    }
//
//    @Value("crawler_data_storage")
//    public static void setDataStorage(String dataStorage) {
//        DATA_STORAGE = dataStorage;
//    }
//    @Value("crawler_data_storage")
//    public static void setCrawlerUrlPath(String crawlerUrlPath) {
//        CRAWLER_URL_PATH = crawlerUrlPath;
//    }
//
//    @Value("crawler_data_storage_file_suffix")
//    public static void setCrawlerDataStorageFileSuffix(String crawlerDataStorageFileSuffix) {
//        CRAWLER_DATA_STORAGE_FILE_SUFFIX = crawlerDataStorageFileSuffix;
//    }
//
//    @Value("crawler_url")
//    public static void setCrawlerUrl(String crawlerUrl) {
//        CRAWLER_URL = crawlerUrl;
//    }
//
//    @Value("crawler_thread_number")
//    public static void setCrawlerThreadNumber(int crawlerThreadNumber) {
//        CRAWLER_THREAD_NUMBER = crawlerThreadNumber;
//    }
}
