package com.pw.tieba.test;

import com.pw.tieba.dao.ForumDao;
import com.pw.tieba.entity.TestUser;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

/**
 * Created by pw on 2017/4/22.
 */
public class SpringJdbcTest {

    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-config.xml");
        ForumDao bean = ctx.getBean(ForumDao.class);
        System.out.println(bean);

        JdbcTemplate jdbc = (JdbcTemplate) ctx.getBean("jdbcTemplate");
        jdbcSelect(jdbc);
//        jdbcSelect(jdbc);
//        jdbcSelect(jdbc);
//        jdbcSelect(jdbc);
//        jdbcSelect(jdbc);
//        jdbcSelect(jdbc);
//        jdbcSelect(jdbc);
//        jdbcSelect(jdbc);
    }

    public static void jdbcSelect(JdbcTemplate jdbc) {
        List query = jdbc.query("SELECT * FROM test_user", new BeanPropertyRowMapper(TestUser.class));
        System.out.println("test_user size ："+query.size());
    }
}
