package com.pw.tieba.test;

import cn.edu.hfut.dmic.webcollector.model.CrawlDatums;
import cn.edu.hfut.dmic.webcollector.model.Page;
import cn.edu.hfut.dmic.webcollector.plugin.berkeley.BreadthCrawler;
import com.alibaba.fastjson.JSON;
import com.pw.tieba.config.Config;
import com.pw.tieba.core.Init;
import com.pw.tieba.dao.ForumDao;
import com.pw.tieba.entity.Forum;
import com.pw.tieba.util.SpringUtil;
import com.pw.tieba.util.StringUtil;
import org.jsoup.select.Elements;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Map;

/**
 * Created by pw on 2017/2/21.
 */
public class Test extends BreadthCrawler {

    private ForumDao forumDao = SpringUtil.getBean(ForumDao.class);

    public void visit(Page page, CrawlDatums crawlDatums) {
        Elements elements = page.getDoc().getElementsByTag("a");
//        getForum(page, crawlDatums);
        System.out.println("\n\n\n ------------------------------------------------------------------------------------------------------->\t\t TITLE:" + page.getDoc().title() + "\t\t URL:" + page.getUrl() + "<-------------------------------------------------------------------------------------------------------");

        String url = page.getUrl();
        if (url.indexOf("https://tieba.baidu.com/f/index") == -1) {
            System.out.println("11");
            Forum forum = getForum(page, crawlDatums);
            if( null != forum) forumDao.inset(forum);
        }
        String html = page.getHtml();
//        System.out.println(html);
//        int indexOf = html.indexOf("PageData.forum");
//        html = html.substring(indexOf, html.length());
//        String json = html.substring(0, html.indexOf(";"));

        System.out.println("\n\n\n");
        for (int i = 0; i < elements.size(); i++) {
            String strUrl = elements.get(i).attr("href");
            if (!"#".equals(strUrl)) {
                if (strUrl.indexOf("tieba.baidu.com") != -1) {

                    crawlDatums.add(strUrl);
                }
                if (strUrl.indexOf("http") == -1) {

                    crawlDatums.add("https://tieba.baidu.com" + strUrl);
                }
            }
        }
    }

    public Test(String crawlPath, boolean autoParse) {
        super(crawlPath, autoParse);
    }

    public static void main(String[] args) throws Exception {
        SpringUtil util = SpringUtil.getSpringUtil();
        JdbcTemplate jdbcTemplate = SpringUtil.getJdbcTemplate();
        ForumDao bean = SpringUtil.getBean(ForumDao.class);
        Forum select = bean.select(1);
        new Init();
        Test test = new Test(Config.CRAWLER_URL_PATH, true);
        test.addSeed("https://tieba.baidu.com");
        test.setThreads(40);
        test.setResumable(true);
        test.start(999999);
    }

    public Forum getForum(Page page, CrawlDatums crawlDatums) {
        String html = page.getHtml();
        int indexOf = html.indexOf("PageData.forum");
        if (indexOf == -1) return null;
        html = html.substring(indexOf + "PageData.forum".length() + 2, html.length());
        String json = html.substring(0, html.indexOf(";"));
        Map<String, Object> parse = (Map<String, Object>) JSON.parse(json);
        parse = StringUtil.jsonUnicodeToChinese(parse);
        String id = parse.get("id").toString();
        if(null != id && !"0".equals(id)){
            Forum forum = new Forum();
            forum.setId(Integer.parseInt(id));
            forum.setInfoJson(JSON.toJSONString(parse));
            forum.setName(parse.get("name").toString());
            forum.setFirstClass(parse.get("first_class").toString());
            forum.setSecondClass(parse.get("second_class").toString());
            return forum;
        }
        return null;
    }

}
