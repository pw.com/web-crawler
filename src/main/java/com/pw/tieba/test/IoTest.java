package com.pw.tieba.test;

import com.gargoylesoftware.htmlunit.DefaultCredentialsProvider;
import com.gargoylesoftware.htmlunit.WebClient;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Created by pw on 2017/2/21.
 */
public class IoTest {

    public static void main(String[] args) throws Exception {
        Proxy httpProxy = new Proxy();
//设置代理服务器地址
        httpProxy.setHttpProxy("180.76.154.5:8888");
        DesiredCapabilities capabilities = DesiredCapabilities.htmlUnit();
        capabilities.setCapability(CapabilityType.PROXY, httpProxy);
        WebDriver driver = new HtmlUnitDriver(capabilities){
            @Override
            protected WebClient modifyWebClient(WebClient client)
            {
                DefaultCredentialsProvider creds = new DefaultCredentialsProvider();
                client.setCredentialsProvider(creds);
                return client;
            }
        };
        driver.get("http://www.baidu.com");
        System.out.println("[" + driver.getTitle() + "]");
    }


}
