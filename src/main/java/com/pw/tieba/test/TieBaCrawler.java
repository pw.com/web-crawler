package com.pw.tieba.test;

import cn.edu.hfut.dmic.webcollector.model.*;
import cn.edu.hfut.dmic.webcollector.net.*;
import cn.edu.hfut.dmic.webcollector.plugin.berkeley.BreadthCrawler;
import org.jsoup.select.Elements;

import java.net.InetSocketAddress;
import java.net.Proxy;

/**
 * Created by pw on 2017/2/15.
 */
public class TieBaCrawler extends BreadthCrawler {

    private final static String URL = "https://tieba.baidu.com";
    static Proxys proxys=new Proxys();

    static TieBaCrawler crawler;



    static {

        proxys.add("183.135.134.204",8998);
        proxys.add("123.96.7.66",3128);
        proxys.add("121.232.144.76",9000);
        proxys.add("117.90.4.150",9000);
        proxys.add("121.232.145.49",9000);
        crawler = new TieBaCrawler("tieba", "www.tieba.com");
    }

    @Override
    public HttpResponse getResponse(CrawlDatum crawlDatum) throws Exception {
        HttpRequest request = new HttpRequest(crawlDatum);
        Proxy proxy = proxys.nextRandom();
        InetSocketAddress address = (InetSocketAddress) proxy.address();
        System.out.println(address.getHostName());
        request.setProxy(proxy);
        return request.getResponse();
    }

    /**
     * @param crawlPath    用于维护URL的文件夹
     * @param downloadPath 用于保存图片的文件夹
     */
    public TieBaCrawler(String crawlPath, String downloadPath) {
        super(crawlPath, true);
    }

    public void visit(Page page, CrawlDatums next) {
        Elements elements = page.getDoc().getElementsByTag("a");
        for (int i = 0 ; i < elements.size() ; i++){
            String strUrl = elements.get(i).attr("href");
            if(!"#".equals(strUrl)){
                if(strUrl.indexOf("tieba.baidu.com") != -1 ){

                    next.add(strUrl);
                }else if (strUrl.indexOf("http") == -1){
                    next.add(URL + strUrl);
                }
            }
        }
    }

    public static void main(String[] args) throws Exception {
        crawler.addSeed(URL);
//        crawler.addRegex("https://tieba.baidu.com/.*");
       /*设置线程数*/
        crawler.setThreads(50);
        /*设置爬虫是否为断点爬取*/
        crawler.setResumable(true);
        crawler.start(5);
    }


}
