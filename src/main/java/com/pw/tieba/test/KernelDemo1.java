package com.pw.tieba.test;

import cn.edu.hfut.dmic.webcollector.fetcher.Executor;
import cn.edu.hfut.dmic.webcollector.model.*;
import cn.edu.hfut.dmic.webcollector.plugin.berkeley.BreadthCrawler;
import cn.edu.hfut.dmic.webcollector.util.CharsetDetector;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by pw on 2017/2/15.
 */
public class KernelDemo1 extends BreadthCrawler {

    private final static String URL = "https://tieba.baidu.com";

    public KernelDemo1(String crawlPath, boolean autoParse){
        super(crawlPath, autoParse);
        this.addSeed(URL);

    }

    public void visit(Page page, CrawlDatums next) {
//        next.add()
    }

    public static void main(String[] args) throws Exception {
        /*定制Executor*/
        Executor executor = new Executor() {



            /*execute应该包含对一个页面从http请求到抽取的过程
              如果在execute中发生异常并抛出，例如http请求超时，
              爬虫会在后面的任务中继续爬取execute失败的任务。
              如果一个任务重试次数太多，超过Config.MAX_EXECUTE_COUNT，
              爬虫会忽略这个任务。Config.MAX_EXECUTE_COUNT的值可以被修改*/
            public void execute(CrawlDatum datum, CrawlDatums next) throws Exception {
                CloseableHttpClient client = HttpClients.createDefault();
                String url = datum.getUrl();
                try {
                    HttpGet get = new HttpGet(url);
                    HttpResponse response = client.execute(get);
                    HttpEntity entity = response.getEntity();
                    /*利用HttpClient获取网页的字节数组，
                      通过CharsetDetector判断网页的编码 */
                    byte[] content = EntityUtils.toByteArray(entity);
                    String charset = CharsetDetector.guessEncoding(content);
                    String html = new String(content, charset);
                    /*利用Jsoup解析网页，并执行抽取等操作*/
                    Document doc = Jsoup.parse(html, url);
                    System.out.println(doc.title());
                    Elements links = doc.select("a[href]");
                    for (int i = 0; i < links.size(); i++) {
                        Element link = links.get(i);
                        /*抽取超链接的绝对路径*/
                        String href = link.attr("abs:href");
                        /*将新链接加入后续任务，这里只加入以http://news.hfut.edu.cn/开头的链接
                          用户不用考虑去重的问题，爬虫内核会自动去重*/
                        if (href.indexOf("tieba.baidu.com") != -1) {
                            next.add(href);
                        } else {
                            next.add(URL + href);
                        }
                    }
                } finally {
                    client.close();
                }
            }
        };


        /*构建一个Crawler*/
        KernelDemo1 crawler = new KernelDemo1("crawl", true);
        /*线程数*/
        crawler.setThreads(100);

        crawler.addSeed(URL);

        //设置爬虫是否以断点模式爬取
        //如果设置为true，爬虫会在启动时继续以前的任务爬取
            //默认为false，如果为false，每次启动爬虫都会重新爬取
        crawler.setResumable(true);

        //爬取3层，层与网站拓扑无关，它是遍历树中的层
        crawler.start(10);

    }
}
